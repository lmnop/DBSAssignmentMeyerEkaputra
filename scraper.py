from bs4 import BeautifulSoup
import requests
import re

# gibt das soup page object zurück
def getPage(url):
	r = requests.get(url)
	data = r.text
	spobj = BeautifulSoup(data, "lxml")
	return spobj

# Findet alle Überschriften der Artikel in dem soup page object
def getHeaders(spobj, headers):
	# Artikel befinden sich innerhalb des <nav> Blockes innerhalb des <div> blockes mit dem Attribut class="keywordliste"
	content = spobj.find("div", { "class" : "keywordliste" }).find("nav")
	# Die Überschrift steht im <header> element
	content = content.find_all("header")
	
	for c in content:
		# Am Ende jeder Überschrift steht noch ein Newline, das wir entfernen wollen
		headers.append(c.text.rstrip())

def main():
	headers = []
	page = 0
	nextPage = True
	# Solange es noch weitere Seiten zum Thema gibt
	while nextPage:
		url = "https://www.heise.de/thema/https?seite=" + str(page)
		spobj = getPage(url)
		getHeaders(spobj, headers)
		# Es gibt noch eine weitere Seite wenn das Element <a> mit attribut class="seite_weiter" exestiert.
		nextPage = spobj.find("a", { "class" : "seite_weiter" } ) is not None
		page += 1
	
	print("Anzahl der Überschriften: " + str(len(headers)))
	words = 0 # Anzahl der Wörter
	dict_words = {} # Dictionary Wort:Anzahl
	for s in headers:
		# Alle Wörter in der Überschrift (nur Zahlen oder Buchstaben)
		for w in re.sub("[^\w]", " ", s).split():
			words += 1
			if w in dict_words:
				dict_words[w] += 1
			else:
				dict_words[w] = 1
				
	print("Anzahl der Wörter: " + str(words))
	print("Anzahl der verschiedenen Wörter: " + str(len(dict_words)))
	print("Top-3 Wörter:")
	for w in sorted(dict_words.items(), key=lambda x: x[1], reverse=True)[:3]:
		print(str(w[0]) + ": " + str(w[1]) + " Vorkomnisse.")

if __name__ == '__main__':
   main()