import tweepy
import csv
import re
import codecs

class StreamListener(tweepy.StreamListener):
	def on_status(self, status):
		if hasattr(status, 'retweeted_status'):
			return True
		row = []
		row.append(status.author.screen_name)
		row.append(status.text)	
		row.append(status.created_at)
		row.append(status.favorite_count)
		row.append(status.retweet_count)
		self.csvw.writerow(row)
		self.count += 1
		if(self.count > 100):
			return False
		
	def on_error(self, status_code):
		if status_code == 420:
			return False

def main():
	consumer_key = ""
	consumer_secret = ""
	access_token = ""
	access_token_secret = ""

	auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_token, access_token_secret)
	
	fobj = codecs.open('twitter-data.csv', mode='w', encoding='utf-8', errors='replace') 
	csvw = csv.writer(fobj, delimiter = ';')
	csvw.writerow(["author", "text", "created_at", "favorite_count", "retweet_count"])
	
	listener = StreamListener()
	listener.csvw = csvw
	listener.count = 0
	stream = tweepy.Stream(auth, listener)
	stream.filter(track=['@realDonaldTrump'])
	fobj.close()   
	

if __name__ == '__main__':
	main()